package com.example.sharedtask;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.widget.ImageButton;

import android.view.View;

import android.content.Intent;

import android.speech.RecognizerIntent;

import java.util.Locale;


public class MainActivity extends AppCompatActivity {
    private ImageButton mShareButton;
    private ImageButton mMicrophoneButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mShareButton = findViewById(R.id.share);
        mShareButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(Intent.ACTION_SEND);

                intent.setType("text/plain");
                String shareBody = "hello";

                intent.putExtra(Intent.EXTRA_TEXT, shareBody);

                startActivity(Intent.createChooser(intent, "Share using"));
            }
        });


    }



}
